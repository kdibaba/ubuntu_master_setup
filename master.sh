#!/usr/bin/env bash

#Add repositories, update and upgrade
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
#sudo add-apt-repository -y ppa:team-xbmc/ppa
sudo add-apt-repository -y ppa:webupd8team/gnome3
sudo add-apt-repository -y ppa:plexapp/plexht
sudo apt-get -y update
sudo apt-get -y upgrade

############################# Install tools

# Install Chrome
sudo apt-get install -y google-chrome-stable

# Install Vim
sudo apt-get install -y vim

# Install VirtualEnv for python
sudo apt-get install -y python-virtualenv
sudo apt-get -y install python-software-properties pkg-config
sudo apt-get -y install software-properties-common
#sudo apt-get -y install xbmc

# Install version controls
sudo apt-get -y install git
sudo apt-get -y install mercurial

# Install Vundle
if [ -d ~/.vim/bundle/vundle ]; then
    echo "Vundle is already imported"
else
    git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
fi

# Install dot_files
if [ -d ~/dot_files ]; then
    echo "dotfiles are already imported so move on."
else
    git clone https://github.com/kdibaba/dot_files.git ~/dot_files
    ln -s -f ~/dot_files/bashrc ~/.bashrc
    ln -s -f ~/dot_files/vimrc ~/.vimrc
    ln -s -f ~/dot_files/netrc ~/.netrc
    ln -s -f ~/dot_files/hgrc ~/.hgrc
    ln -s -f ~/dot_files/wallpaper ~/.wallpaper
fi

# install Gnome
#sudo apt-get -y install gnome
#sudo apt-get -y autoremove

#Install byobu
sudo apt-get -y install byobu

# Install sqlite3 to abe able to edit sqlite in dev environment
sudo apt-get install -y sqlite3

# install some common apps I always get
sudo apt-get install -y vlc
sudo apt-get install -y filezilla
sudo apt-get install -y htop
sudo apt-get install -y tree
sudo apt-get install -y grive
sudo apt-get install -y xfonts-terminus xfonts-terminus-dos xfonts-terminus-oblique
sudo apt-get install -y cifs-utils
sudo apt-get install -y aptitude
sudo apt-get install -y plexhometheater

# Compiz plugin to use the put command that lets you move
# you move windows between monitors
sudo apt-get install compizconfig-settings-manager
sudo apt-get install compiz-plugins

# Configure tools and settings
git config --global user.name "kdibaba"
git config --global user.email "kdibaba@gmail.com"
git config --global credential.helper "cache --timeout=259200"
git config --global push.default simple
git config --global alias.ci 'commit -a'

#Configure touchpad for 2 finger scrolling
synclient VertTwoFingerScroll=1
synclient HorizTwoFingerScroll=1

# Import development projects. Run the script to do this.
sh ~/ubuntu_master_setup/projects.sh
