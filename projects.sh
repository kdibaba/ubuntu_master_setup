#!/bin/bash
if [ -d ~/projects/rbm/ ]; then
    echo "rbm is already imported"
else
    hg clone https://kdibaba:Ken22enidib@bitbucket.org/kdibaba/rbm ~/projects/rbm
fi

if [ -d ~/projects/shoe_chrome_extension/ ]; then
    echo "The shoe_chrome_extenstion is already imported"
else
    git clone https://kdibaba:Ken22enidib@bitbucket.org/kdibaba/shoe_chrome_extension.git ~/projects/shoe_chrome_extension
fi

if [ -d ~/projects/media_organizer/ ]; then
    echo "Media organizer code is already imported"
else
    git clone https://kdibaba:Ken22enidib@bitbucket.org/kdibaba/media_organizer.git ~/projects/media_organizer
fi

if [ -d ~/projects/sam/ ]; then
    echo "The movie filter chrome extension is already imported"
else
    git clone https://kdibaba:Ken22enidib@bitbucket.org/kdibaba/sam.git ~/projects/sam
fi
